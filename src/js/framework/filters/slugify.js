export default (input = '') =>
    input ? input.toLowerCase().replace(/ /g, '-') : '';
