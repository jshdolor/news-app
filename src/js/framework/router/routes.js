import NewsListPage from '~/framework/components/modules/newsList';
import NewsDetailsPage from '~/framework/components/modules/newsDetails';
import requirePresetFilters from '~/framework/middleware/requirePresetFilters';
export default [
    {
        name: 'home-page',
        path: '/',
        component: NewsListPage,
    },
    {
        name: 'news-details-page',
        path: '/details',
        component: NewsDetailsPage,
        props: true,
        beforeEnter: (to, from, next) => {
            requirePresetFilters(to, from, next);
        },
    },
];
