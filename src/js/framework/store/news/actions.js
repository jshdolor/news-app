export default {
    setSelectedNews({ commit }, data) {
        commit('setSelectedNews', data);
    },
};
