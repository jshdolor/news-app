export default {
    setKeyword({ commit }, data) {
        commit('setKeyword', data);
    },
    setCountry({ commit }, data) {
        commit('setCountry', data);
    },
    setCategory({ commit }, data) {
        commit('setCategory', data);
    },
    setFilters({ commit }, data) {
        const { q, country, category } = data;
        commit('setKeyword', q);
        commit('setCountry', country);
        commit('setCategory', category);
    },
};
