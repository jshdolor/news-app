export default {
    setKeyword(state, data = '') {
        state.q = encodeURI(data);
    },
    setCountry(state, data) {
        state.country = data;
    },
    setCategory(state, data) {
        state.category = data;
    },
};
