import { defaultFilters } from '~/application/config';

export default {
    ...defaultFilters,
};
