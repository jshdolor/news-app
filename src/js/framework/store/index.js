import Vuex from 'vuex';
import Vue from 'vue';
Vue.use(Vuex);

import filter from './filter';
import news from './news';

export default new Vuex.Store({
    modules: {
        filter,
        news,
    },
});
