import Vue from 'vue';
import App from '~/framework/components/App';
import Router from '~/framework/router';

import store from '~/framework/store';
import filters from '~/framework/filters';

Vue.config.productionTip = false;

import Toasted from 'vue-toasted';

import '~/../css/main.css';

class Bootstrap {
    init() {
        Vue.use(Toasted);
        Vue.use(filters);

        const app = new Vue({
            store,
            router: new Router().boot(),
            render: (h) => h(App),
        });

        app.$mount('#app');
        window.$app = app;
    }
}

export default Bootstrap;
