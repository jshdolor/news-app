const requirePresetFilters = (to, from, next, redirect = 'home-page') => {
    const { category, q, country } = to?.query;
    if (!category || !q || !country) {
        next({ name: redirect });
    }
    next();
};

export default requirePresetFilters;
