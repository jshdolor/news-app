const isUndefined = (test) => {
    return typeof test === 'undefined';
};

const isEmpty = (n) => {
    return isUndefined(n) || n === '' || n === null || n === 0;
};

const mergeDeep = (target, source) => {
    for (const key of Object.keys(source)) {
        if (source[key] instanceof Object)
            Object.assign(source[key], mergeDeep(target[key], source[key]));
    }

    Object.assign(target || {}, source);
    return target;
};

export { isEmpty, mergeDeep };
