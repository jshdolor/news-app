import { format } from 'date-fns';
import { dateFormat, imagePlaceholder } from '~/application/config';
import slugify from '~/framework/filters/slugify';

class Article {
    constructor(data = {}) {
        this._author = data.author;
        this._title = data.title;
        this._description = data.description;
        this._image = data.urlToImage;
        this._publishedAt = data.publishedAt;
        this._content = data.content;
    }

    get slug() {
        const date = new Date(this._publishedAt);
        return `${slugify(this._title)}-${date.getTime()}`;
    }

    get author() {
        return this._author;
    }

    get title() {
        return this._title;
    }

    get description() {
        return this._description;
    }

    get image() {
        return this._image || imagePlaceholder;
    }

    set image(value) {
        this._image = value;
    }

    get content() {
        return this._content;
    }

    get publishedAt() {
        return format(new Date(this._publishedAt), dateFormat);
    }
}
export default Article;
