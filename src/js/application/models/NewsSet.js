import ArticleModel from './Article';

class NewsSet {
    constructor(data = {}) {
        this._total = data.totalResults || 0;
        this._articles = (data.articles || []).map(
            (article) => new ArticleModel(article)
        );
    }

    get articles() {
        return this._articles;
    }

    set articles(value = []) {
        this._articles = value;
    }

    get total() {
        return this._total;
    }
}
export default NewsSet;
