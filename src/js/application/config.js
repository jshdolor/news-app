export const projectName = process.env.VUE_APP_PROJECT_NAME;

export const newsApi = {
    endpoint: process.env.VUE_APP_NEWS_ENDPOINT,
    apiKey: process.env.VUE_APP_NEWS_API_KEY,
};

export const dateFormat = 'PPPppp';

export const defaultFilters = {
    category: 'general',
    country: 'ph',
    q: '',
};

export const imagePlaceholder =
    'https://via.placeholder.com/400x300.png?text=Image+Not+Available';
