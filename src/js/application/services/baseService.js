import ExceptionHandler from '~/infrastructure/exceptions/handler';

export default class BaseService {
    static async handleAsync(serviceCall, modeler = null) {
        try {
            const data = await serviceCall;
            if (typeof modeler === 'function') {
                return modeler(data);
            }
            return data;
        } catch (error) {
            return new ExceptionHandler(error);
        }
    }
}
