import ApiClient from '~/infrastructure/client/newsApiClient';
import BaseService from '~/application/services/baseService';

import NewsSetModel from '~/application/models/NewsSet';
import ArticleModel from '~/application/models/Article';

import CacheManager from '~/infrastructure/cache';

class NewsService extends BaseService {
    static async getArticleByTitle(params) {
        return this.handleAsync(
            ApiClient.setUrl('/top-headlines').get(params),
            ({ articles = [] }) => {
                if (articles[0]) {
                    const article = new ArticleModel(articles[0]);
                    return article;
                }

                throw 'News not found';
            }
        );
    }

    static headlines(params = {}) {
        const cacheKey = JSON.stringify(params);

        if (CacheManager.has(cacheKey)) {
            return new Promise((res) => res(CacheManager.get(cacheKey)));
        }

        return this.handleAsync(
            ApiClient.setUrl('/top-headlines').get(params),
            (data) => {
                const newsSet = new NewsSetModel(data);
                CacheManager.set(cacheKey, newsSet);
                return newsSet;
            }
        );
    }
}

export default NewsService;
