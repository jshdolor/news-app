class UnhandledException {
    constructor({ error = {}, message = 'An error has occured' } = {}) {
        this.error = error;
        this._errorMessages = message;
    }

    get errorMessages() {
        return this._errorMessages;
    }

    run() {
        window.$app.$toasted.error(this.errorMessages, {
            duration: 3000,
            position: 'bottom-center',
        });
        throw new Error(this.errorMessages);
    }
}

export default UnhandledException;
