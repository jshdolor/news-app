import ExceptionFactory from '~/infrastructure/factories/exception';

class ExceptionHandler {
    constructor(error, code = null) {
        const statusCode = error?.response?.status || code;

        const errorData = error?.response?.data || { message: error };

        const exception = ExceptionFactory.make(statusCode, errorData);

        if (exception.isContinuous) {
            return exception;
        }
    }
}

export default ExceptionHandler;
