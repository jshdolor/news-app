class NotFoundException {
    constructor({ error = {}, message = 'Not Found' } = {}) {
        this.error = error;
        this._errorMessages = error.message || message;
    }

    get errorMessages() {
        return this._errorMessages;
    }
}

export default NotFoundException;
