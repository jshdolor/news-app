import Http from '~/infrastructure/client/http';
import { newsApi } from '~/application/config';

const { apiKey, endpoint } = newsApi;

export default class ApiClient {
    static url = '';
    static options = {
        headers: {
            'x-api-key': apiKey,
        },
    };

    static setUrl(url) {
        this.url = `${endpoint}${url}`;
        return ApiClient;
    }

    static get(params, options = {}) {
        let http = new Http();
        options = { ...this.options, ...options };
        return http.get(this.url, params, options);
    }
}
