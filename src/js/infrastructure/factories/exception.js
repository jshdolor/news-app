import UnhandledException from '~/infrastructure/exceptions/unhandledException';
import NotFoundException from '~/infrastructure/exceptions/notFoundException';

class ExceptionFactory {
    static make(errCode = 500, error) {
        const exceptionClsMapping = {
            404: NotFoundException,
        };

        if (exceptionClsMapping[errCode]) {
            const exception = new exceptionClsMapping[errCode]({ error });
            return exception;
        }

        return new UnhandledException(error).run();
    }
}

export default ExceptionFactory;
