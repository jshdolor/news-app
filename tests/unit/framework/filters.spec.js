import Filters from '~/framework/filters';
import { expect } from 'chai';

describe('Framework Filters', () => {
    it('should have a method that register filters ', () => {
        expect(Filters).to.have.property('install');
    });
});
