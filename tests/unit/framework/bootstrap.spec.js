import Boot from '~/framework/bootstrap';
import { expect } from 'chai';

describe('bootstrap', () => {
    let App = null;
    beforeEach(() => {
        App = new Boot();
    });

    it('should have a method to initialize the main app', () => {
        expect(App).to.have.property('init');
    });

    it('should make the main vue app available globally', () => {
        expect(window.$app).to.be.not.null;
    });
});
