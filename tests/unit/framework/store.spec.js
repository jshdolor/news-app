import store from '~/framework/store';
import { shallowMount } from '@vue/test-utils';
// import { expect, util } from 'chai';
import { defaultFilters } from '~/application/config';
// import chai, { expect } from 'chai';
// import sinon from 'sinon';
// import sinonChai from 'sinon-chai';

// chai.use(sinonChai);

import App from '~/framework/components/App';
import { expect } from 'chai';

describe('store', () => {
    let app;
    beforeEach(() => {
        app = shallowMount(App, { store });
        app = app.vm;
    });

    it('filter namespace should have the default values from config', () => {
        expect(app.$store.state.filter).to.deep.equal(defaultFilters);
    });

    it('filter - setKeyword to set the q property of the filter namespace ', () => {
        const keyword = 'test';
        app.$store.dispatch('filter/setKeyword', keyword);
        expect(app.$store.state.filter.q).to.be.equal(keyword);
    });

    it('filter - setCategory to set the category property of the filter namespace ', () => {
        const category = 'health';
        app.$store.dispatch('filter/setCategory', category);
        expect(app.$store.state.filter.category).to.be.equal(category);
    });

    it('filter - setCountry to set the country property of the filter namespace ', () => {
        const country = 'us';
        app.$store.dispatch('filter/setCountry', country);
        expect(app.$store.state.filter.country).to.be.equal(country);
    });

    it('filter - setFilters to set the whole filter namespace ', () => {
        const query = {
            q: 'test',
            country: 'us',
            category: 'health',
        };
        app.$store.dispatch('filter/setFilters', query);
        expect(app.$store.state.filter).to.deep.equal(query);
    });
});
