import * as config from '~/framework/config';
import { expect } from 'chai';

describe('Framework Config', () => {
    it('should have a property "router" ', () => {
        expect(config).to.have.property('router');
    });
});
