import { isEmpty, mergeDeep } from '~/framework/helpers';
import { expect } from 'chai';

describe('framework helpers', () => {
    it('isEmpty function should be identify if parameter passed is empty', () => {
        expect(isEmpty(false)).to.be.false;
        expect(isEmpty(null)).to.be.true;
        expect(isEmpty()).to.be.true;
        expect(isEmpty([1, 2, 3])).to.be.false;
        expect(isEmpty({ foo: { bar: {} } })).to.be.false;
    });

    it('mergeDeep function should be merge deep obj', () => {
        let testObj = {
            foo: {
                bar: 1,
            },
        };

        let testObj2 = {
            foo: {
                test: 1,
            },
        };
        expect(mergeDeep(testObj, testObj2)).to.have.nested.property(
            'foo.test'
        );
    });
});
