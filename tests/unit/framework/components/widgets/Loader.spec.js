import { shallowMount, mount } from '@vue/test-utils';
import { expect } from 'chai';
import Loader from '~/framework/components/widgets/Loader';

describe('Loader component', () => {
    it('should render correctly', () => {
        let wrapper = shallowMount(Loader);
        expect(wrapper.find('.loader').exists()).to.be.true;
    });

    it('should be hidden when propsData show is false', () => {
        let wrapper = mount(Loader, {
            propsData: {
                show: false,
            },
        });

        const element = wrapper;

        expect(element.attributes('style')).to.be.equal('display: none;');
    });
});
