import { shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import Header from '~/framework/components/layouts/Header';
import MainLayout from '~/framework/components/layouts/Main';

describe('Main Layout component', () => {
    it('should have the header component', () => {
        let wrapper = shallowMount(MainLayout, {
            stubs: ['router-view'],
        });
        expect(wrapper.findComponent(Header).exists()).to.be.true;
    });
});
