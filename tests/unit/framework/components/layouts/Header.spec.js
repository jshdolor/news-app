import { shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import Header from '~/framework/components/layouts/Header';

import { projectName } from '~/application/config';

describe('Header Layout component', () => {
    it('should render correctly', () => {
        let wrapper = shallowMount(Header, { stubs: ['router-link'] });
        expect(wrapper.find('.header-container').exists()).to.be.true;
    });

    it('should have the title of the app in its component', () => {
        let wrapper = shallowMount(Header, { stubs: ['router-link'] });

        expect(wrapper.find('.header-container').text()).to.contains(
            projectName
        );
    });
});
