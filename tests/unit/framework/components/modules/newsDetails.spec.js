// eslint-disable-next-line no-unused-vars
import { shallowMount, mount } from '@vue/test-utils';
// eslint-disable-next-line no-unused-vars
import { expect } from 'chai';
import NewsDetails from '~/framework/components/modules/newsDetails';
import store from '~/framework/store';

const $route = {
    path: '/details',
    query: {
        category: 'general',
        country: 'ph',
        q:
            'Every camera on the Mars Perseverance rover from NASA explained - Digital Camera World',
    },
};

import ArticleModel from '~/application/models/Article';
import NewsService from '~/application/services/news';

const dummyData = {
    source: { id: null, name: 'Inquirer.net' },
    author: 'Maricar Cinco',
    title: 'CoronaVac rollout begins; AstraZeneca delayed - INQUIRER.net',
    description:
        'The Philippines begins its COVID-19 vaccination drive on Monday with the first 600,000 doses of the donated Chinese vaccine CoronaVac in major government hospitals in Metro Manila.\r\n\r\nThe country',
    url: 'https://www.inquirer.net',
    urlToImage:
        'https://newsinfo.inquirer.net/files/2021/03/Front-Page128123-620x427.jpg',
    publishedAt: '2021-02-28T21:00:00Z',
    content:
        'FINALLY The Philippines vaccination drive starts on Monday with the delivery of the initial 600,000 doses of the China-made CoronaVac vaccine, here being unloaded from a Chinese military plane and lo… [+7798 chars]',
};

const modeledData = new ArticleModel(dummyData);
NewsService.getArticleByTitle = () => {
    return Promise.resolve(modeledData);
};

describe('News Details Page', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = mount(NewsDetails, {
            mocks: {
                $route,
            },
            data() {
                return {
                    searchedData: modeledData,
                };
            },
            store,
        });
    });

    it('should have and display the snews image', () => {
        const img = wrapper.find('.news-details-container img');
        expect(img.exists()).to.be.true;
    });

    it('should have the correct src attribute for the news image', () => {
        const img = wrapper.find('.news-details-container img');

        expect(img.attributes()['src']).to.be.equal(modeledData.image);
    });

    it('should have and display the news title', () => {
        const element = wrapper.find('.news-details-container .title');
        expect(element.exists()).to.be.true;
    });

    it('should have the correct title for the current news', () => {
        const element = wrapper.find('.title');

        expect(element.text()).to.be.equal(modeledData.title);
    });

    it('should have and display the news author', () => {
        const element = wrapper.find('.news-details-container .subtitle');
        expect(element.exists()).to.be.true;
    });

    it('should have the correct author for the current news', () => {
        const element = wrapper.find('.subtitle');

        expect(element.text()).to.be.equal(modeledData.author);
    });

    it('should have and display the news content', () => {
        const element = wrapper.find('.news-details-container .content');
        expect(element.exists()).to.be.true;
    });

    it('should have the correct content for the current news', () => {
        const element = wrapper.find('.content');

        expect(element.text()).to.be.equal(modeledData.content);
    });

    it('should have and display the news content', () => {
        const element = wrapper.find('.news-details-container .publish-date');
        expect(element.exists()).to.be.true;
    });

    it('should have the correct content for the current news', () => {
        const element = wrapper.find('.publish-date');

        expect(element.text()).to.be.equal(modeledData.publishedAt);
    });
});
