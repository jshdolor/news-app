import { createLocalVue, mount } from '@vue/test-utils';
import { expect } from 'chai';
import Filter from '~/framework/components/modules/newsList/Filter';
import store from '~/framework/store';

import CountryDropdown from '~/framework/components/modules/newsList/CountryDropdown';
import CategoryDropdown from '~/framework/components/modules/newsList/CategoryDropdown';
import SearchTextField from '~/framework/components/modules/newsList/SearchTextField';

import filters from '~/framework/filters';
const localVue = createLocalVue();
filters.install(localVue);

describe('News List Filter component', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = mount(Filter, {
            provide: { isFetching: () => true },
            store,
            localVue,
        });
    });

    it('should have the CountryDropdown component', () => {
        expect(wrapper.findComponent(CountryDropdown).exists()).to.be.true;
    });

    it('should have the CategoryDropdown component', () => {
        expect(wrapper.findComponent(CategoryDropdown).exists()).to.be.true;
    });

    it('should have the SearchTextField component', () => {
        expect(wrapper.findComponent(SearchTextField).exists()).to.be.true;
    });
});
