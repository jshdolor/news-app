import CountryDropdown from '~/framework/components/modules/newsList/CountryDropdown';
import { mount } from '@vue/test-utils';
import { expect } from 'chai';
import store from '~/framework/store';

describe('News List CountryDropdown component', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = mount(CountryDropdown, { store });
    });

    it('should should change the country value in store filter namespace', async () => {
        const country = 'us';
        const dropdown = wrapper.find('.field');
        dropdown.element.value = country;
        dropdown.trigger('change');

        expect(wrapper.vm.$store.state.filter.country).to.be.equal(country);
    });
});
