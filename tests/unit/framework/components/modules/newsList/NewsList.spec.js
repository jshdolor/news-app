// eslint-disable-next-line no-unused-vars
import { createLocalVue, shallowMount, mount, render } from '@vue/test-utils';
import { expect } from 'chai';

import NewsList from '~/framework/components/modules/newsList';
import Filter from '~/framework/components/modules/newsList/Filter';

import store from '~/framework/store';
import filters from '~/framework/filters';
const localVue = createLocalVue();
filters.install(localVue);

describe('News List Page', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = mount(NewsList, {
            store,
            localVue,
        });
    });

    it('should hide news count if articles fetch is less than 1', () => {
        const element = wrapper.find('.news-list-page .total-news-count');
        wrapper.vm.totalNews = 0;
        expect(element.attributes('style')).to.be.equal('display: none;');
    });

    it('should show and display number of news available if greater than 0', (done) => {
        const element = wrapper.find('.news-list-page .total-news-count');
        setTimeout(() => {
            if (wrapper.vm.totalNews > 0)
                expect(element.attributes('style')).to.be.not.equal(
                    'display: none;'
                );
            done();
        }, 1500);
    });

    it('should contain the filters component', () => {
        expect(wrapper.findComponent(Filter).exists()).to.be.true;
    });
});
