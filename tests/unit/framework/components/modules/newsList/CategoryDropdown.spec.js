import CategoryDropdown from '~/framework/components/modules/newsList/CategoryDropdown';
import { createLocalVue, mount } from '@vue/test-utils';
import { expect } from 'chai';
import store from '~/framework/store';
import filters from '~/framework/filters';
const localVue = createLocalVue();
filters.install(localVue);

describe('News List CategoryDropdown component', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = mount(CategoryDropdown, { store, localVue });
    });

    it('should should change the category value in store filter namespace', async () => {
        const category = 'science';
        const dropdown = wrapper.find('.field');
        dropdown.element.value = category;
        dropdown.trigger('change');

        expect(wrapper.vm.$store.state.filter.category).to.be.equal(category);
    });
});
