import SearchTextField from '~/framework/components/modules/newsList/SearchTextField';
import { mount } from '@vue/test-utils';
import { expect } from 'chai';
import store from '~/framework/store';
import flushPromises from 'flush-promises';

describe('News List SearchTextField component', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = mount(SearchTextField, { store });
    });

    it('should should change the q value in store filter namespace', async () => {
        const keyword = 'TEST';
        const input = wrapper.find('input');
        input.element.value = keyword;
        input.trigger('input');

        //wait for the timeout count
        flushPromises().then(() => {
            expect(wrapper.vm.$store.state.filter.q).to.be.equal(keyword);
        });
    });
});
