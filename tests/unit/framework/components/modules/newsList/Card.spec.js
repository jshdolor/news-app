import Card from '~/framework/components/modules/newsList/Card';
import { mount } from '@vue/test-utils';
import { expect } from 'chai';
import { format } from 'date-fns';
import { dateFormat } from '~/application/config';

import ArticleModel from '~/application/models/Article';

const dummyData = {
    source: { id: null, name: 'Inquirer.net' },
    author: 'Maricar Cinco',
    title: 'CoronaVac rollout begins; AstraZeneca delayed - INQUIRER.net',
    description:
        'The Philippines begins its COVID-19 vaccination drive on Monday with the first 600,000 doses of the donated Chinese vaccine CoronaVac in major government hospitals in Metro Manila.\r\n\r\nThe country',
    url: 'https://www.inquirer.net',
    urlToImage:
        'https://newsinfo.inquirer.net/files/2021/03/Front-Page128123-620x427.jpg',
    publishedAt: '2021-02-28T21:00:00Z',
    content:
        'FINALLY The Philippines vaccination drive starts on Monday with the delivery of the initial 600,000 doses of the China-made CoronaVac vaccine, here being unloaded from a Chinese military plane and lo… [+7798 chars]',
};

const news = new ArticleModel(dummyData);

describe('News List Card component', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = mount(Card, {
            propsData: {
                news,
            },
        });
    });

    it('should should show the news image with the correct url', () => {
        const img = wrapper.find('img');
        expect(img.exists()).to.be.true;
        expect(img.attributes()['src']).to.be.equal(dummyData.urlToImage);
    });

    it('should should show the news title with the dummy title', () => {
        const element = wrapper.find('.title');
        expect(element.exists()).to.be.true;
        expect(element.text()).to.be.equal(dummyData.title);
    });

    it('should should show the news author with the dummy author', () => {
        const element = wrapper.find('.subtitle');
        expect(element.exists()).to.be.true;
        expect(element.text()).to.be.equal(dummyData.author);
    });

    it('should should show the news publishedAt with the dummy publishedAt', () => {
        const element = wrapper.find('.publish-date');
        expect(element.exists()).to.be.true;

        const formattedDate = format(
            new Date(dummyData.publishedAt),
            dateFormat
        );

        expect(element.text()).to.be.equal(formattedDate);
    });
});
