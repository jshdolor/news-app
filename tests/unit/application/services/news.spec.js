import NewsService from '~/application/services/news';
import NewsSetModel from '~/application/models/NewsSet';
import ArticleModel from '~/application/models/Article';
import { expect } from 'chai';

import { defaultFilters } from '~/application/config';

describe('News Service', () => {
    it('should have the handleAsync method', () => {
        expect(NewsService).to.have.property('handleAsync');
    });

    it('should have the getArticleByTitle method', () => {
        expect(NewsService).to.have.property('getArticleByTitle');
    });

    it('should have the headlines method', () => {
        expect(NewsService).to.have.property('headlines');
    });

    it('headlines method should return a NewsSet Object', async () => {
        const response = await NewsService.headlines(defaultFilters);
        expect(response instanceof NewsSetModel).to.be.true;
    });

    it('getArticleByTitle method should return an Article Object', async () => {
        const response = await NewsService.getArticleByTitle({
            ...defaultFilters,
            q: 'gma',
        });
        expect(response instanceof ArticleModel).to.be.true;
    });
});
