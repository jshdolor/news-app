import * as constants from '~/application/constants';
import { expect } from 'chai';

describe('Application Constants', () => {
    it('should have a property "countries" ', () => {
        expect(constants).to.have.property('countries');
    });

    it('should have a property "categories" ', () => {
        expect(constants).to.have.property('categories');
    });
});
