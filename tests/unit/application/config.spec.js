import * as config from '~/application/config';
import { expect } from 'chai';

describe('Application Config', () => {
    it('should have a property "projectName" ', () => {
        expect(config).to.have.property('projectName');
    });

    it('should have a property "newsApi" ', () => {
        expect(config).to.have.property('newsApi');
    });

    it('should have a property "dateFormat" ', () => {
        expect(config).to.have.property('dateFormat');
    });

    it('should have a property "defaultFilters" ', () => {
        expect(config).to.have.property('defaultFilters');
    });

    it('should have a property "imagePlaceholder" ', () => {
        expect(config).to.have.property('imagePlaceholder');
    });
});
