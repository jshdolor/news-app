import NewsSetModel from '~/application/models/NewsSet';
import { expect } from 'chai';

const dummyNewsSet = {
    status: 'ok',
    totalResults: 1,
    articles: [
        {
            source: { id: null, name: 'GMA News' },
            author: '',
            title: 'Upcoming dry season may be rainier than usual - GMA News',
            description:
                'The Philippines may experience an unusual dry season this year due to rains that may occur more than usual, GMA&rsquo;s resident meteorologist Nathaniel &ldquo;Mang Tani&rdquo; Cruz said Thursday.',
            url:
                'https://www.gmanetwork.com/news/scitech/weather/777447/upcoming-dry-season-may-be-rainier-than-usual/story/',
            urlToImage:
                'https://images.gmanews.tv/webpics/2019/01/rain_traffic_2019_01_06_18_44_49.jpg',
            publishedAt: '2021-02-25T12:25:20Z',
            content:
                'The Philippines may experience an unusual dry season this year due to rains that may occur more than usual, GMA’s resident meteorologist Nathaniel “Mang Tani” Cruz said Thursday.\r\n Citing data from P… [+646 chars]',
        },
    ],
};

describe('Model: News Set', () => {
    let newsSet = null;
    beforeEach(() => {
        newsSet = new NewsSetModel(dummyNewsSet);
    });

    it('should have a property "articles"', () => {
        expect(newsSet).to.have.property('articles');
        expect(newsSet.articles.length).to.be.equal(
            dummyNewsSet.articles.length
        );
    });

    it('should have a property "total"', () => {
        expect(newsSet).to.have.property('total');
        expect(newsSet.total).to.be.equal(dummyNewsSet.totalResults);
    });
});
