import ArticleModel from '~/application/models/Article';
import { expect } from 'chai';

const dummyArticle = {
    source: {
        id: null,
        name: 'GMA News',
    },
    author: 'Author',
    title: 'Upcoming dry season may be rainier than usual - GMA News',
    description:
        'The Philippines may experience an unusual dry season this year due to rains that may occur more than usual, GMA&rsquo;s resident meteorologist Nathaniel &ldquo;Mang Tani&rdquo; Cruz said Thursday.',
    url:
        'https://www.gmanetwork.com/news/scitech/weather/777447/upcoming-dry-season-may-be-rainier-than-usual/story/',
    urlToImage:
        'https://images.gmanews.tv/webpics/2019/01/rain_traffic_2019_01_06_18_44_49.jpg',
    publishedAt: '2021-02-25T12:25:20Z',
    content:
        'The Philippines may experience an unusual dry season this year due to rains that may occur more than usual, GMA’s resident meteorologist Nathaniel “Mang Tani” Cruz said Thursday.\r\n Citing data from P… [+646 chars]',
};

describe('Model: Article', () => {
    let article = null;
    beforeEach(() => {
        article = new ArticleModel(dummyArticle);
    });

    it('should have a property "author" and be of correct value', () => {
        expect(article).to.have.property('author');
        expect(article.author).to.be.equal('Author');
    });

    it('should have a property "title" and be of correct value ', () => {
        expect(article).to.have.property('title');
        expect(article.title).to.be.equal(dummyArticle.title);
    });

    it('should have a property "description" and be of correct value ', () => {
        expect(article).to.have.property('description');
        expect(article.description).to.be.equal(dummyArticle.description);
    });

    it('should have a property "image" and be of correct value ', () => {
        expect(article).to.have.property('image');
        expect(article.image).to.be.equal(dummyArticle.urlToImage);
    });

    it('should have a property "content"  and be of correct value ', () => {
        expect(article).to.have.property('content');
        expect(article.content).to.be.equal(dummyArticle.content);
    });

    it('should have a property "publishedAt" and be of correct value  ', () => {
        expect(article).to.have.property('publishedAt');
        expect(article.publishedAt).to.be.equal(
            'February 25th, 2021 at 8:25:20 PM GMT+8'
        );
    });
});
