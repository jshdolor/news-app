import CacheManager from '~/infrastructure/cache';
import { expect } from 'chai';

describe('CacheManager', () => {
    const testObject = { foo: { bar: 1 } };
    const testObjectKey = 'TEST';

    beforeEach(() => {
        CacheManager.set(testObjectKey, testObject);
    });

    it('should save value on cache using key-value pair', () => {
        expect(CacheManager.has(testObjectKey)).to.be.true;
    });

    it('should give out the correct value given the key', () => {
        expect(CacheManager.get(testObjectKey)).to.deep.equal(testObject);
    });
});
