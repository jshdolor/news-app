import Http from '~/infrastructure/client/http';
import { expect } from 'chai';

describe('HttpClient', () => {
    let client = null;
    beforeEach(() => {
        client = new Http();
    });

    it('should have a function "get" that returns a promise', () => {
        expect(client.get()).to.have.property('then');
    });
});
