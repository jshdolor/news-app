import NewsApiClient from '~/infrastructure/client/newsApiClient';
import { expect } from 'chai';
import { newsApi } from '~/application/config';
const { endpoint } = newsApi;

describe('NewsApiClient', () => {
    it('should have a function that sets the url with the news api endpoint', () => {
        const testPath = '/everything';
        expect(NewsApiClient.setUrl(testPath).url).to.be.equal(
            `${endpoint}${testPath}`
        );
    });

    it('should have a function "get" that returns a promise', () => {
        expect(NewsApiClient.get()).to.have.property('then');
    });
});
